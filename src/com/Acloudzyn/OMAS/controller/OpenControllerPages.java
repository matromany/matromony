package com.Acloudzyn.OMAS.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OpenControllerPages {
	
	@RequestMapping("/")
	public String indexPage()
	{
		System.out.println("hello");
		System.out.println("In controller");
		return "indexPage";
		//return "redirect:/header.jsp";
	}
	
	@RequestMapping(value="/about")
	public String aboutPage()
	{
		System.out.println("In about");
		return "about";
	}
	
	@RequestMapping(value="/indexPage")
	public String indexPage1()
	{
		System.out.println("In controller");
		return "indexPage";
	}
	
	@RequestMapping(value="/contact")
	public String contact()
	{
		System.out.println("In contact");
		return "contact";
	}
	@RequestMapping(value="/search")
	public String search()
	{
		System.out.println("In search");
		return "search";
	}
	@RequestMapping(value="/app")
	public String mobileApp()
	{
		System.out.println("In app");
		return "app";
	}

}
