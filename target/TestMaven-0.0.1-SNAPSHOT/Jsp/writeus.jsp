<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<!-- html -->
<html>
<!-- head -->
<head>
<title>Match a Matrimonial Category Bootstrap Responsive Web Template | write to us :: w3layouts</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /><!-- bootstrap-CSS -->
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" /><!-- Fontawesome-CSS -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type='text/javascript' src='js/jquery-2.2.3.min.js'></script>
<!-- Custom Theme files -->
<link href="css/menu.css" rel="stylesheet" type="text/css" media="all" /> <!-- menu style --> 
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<!--meta data-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Match Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--//meta data-->
<!-- online fonts -->
<link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&amp;subset=devanagari,latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<!-- /online fonts -->
<!-- nav smooth scroll -->
<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<!-- //nav smooth scroll -->
</head>
<!-- //head -->
<!-- body -->
<body>

<!-- header -->
<header>
	<!--  Navigation Start -->
 <div class="navbar navbar-inverse-blue navbar">
    <!--<div class="navbar navbar-inverse-blue navbar-fixed-top">-->
      <div class="navbar-inner">
        <div class="container">
          <div class="menu">
					<div class="cd-dropdown-wrapper">
						<a class="cd-dropdown-trigger" href="#0">Browse Profiles by</a>
						<nav class="cd-dropdown"> 
							<a href="#0" class="cd-close">Close</a>
							<ul class="cd-dropdown-content"> 
								<li><a href="matches.jsp">All Profiles</a></li>
								<li class="has-children">
									<a href="#">Mother Tongue</a> 
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
												<li><a href="l_list.jsp">Language 1</a></li>
												<li><a href="l_list.jsp">Language 2</a> </li>
												<li><a href="l_list.jsp">Language 3</a></li>
												<li><a href="l_list.jsp">Language 4</a></li>
												<li><a href="l_list.jsp">Language 5</a></li> 
												<li><a href="l_list.jsp">Language 6</a></li> 
												<li><a href="l_list.jsp">Language 7</a></li> 
												<li><a href="l_list.jsp">Language 8</a></li> 
												<li><a href="l_list.jsp">Language 9</a></li> 
												<li><a href="l_list.jsp">Language 10</a></li> 
												<li><a href="l_list.jsp">Language 11</a></li> 
												<li><a href="l_list.jsp">Language 12</a></li> 
												<li><a href="l_list.jsp">Language 13</a></li> 
												<li><a href="l_list.jsp">Language 14</a></li> 
												<li><a href="l_list.jsp">Language 15</a></li> 
												<li><a href="l_list.jsp">Language 16</a></li> 
												<li><a href="l_list.jsp">Language 17</a></li> 
												<li><a href="l_list.jsp">Language 18</a></li> 
												<li><a href="l_list.jsp">Language 19</a></li> 
												<li><a href="l_list.jsp">Language 20</a></li> 
											
									</ul> <!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children -->
								<li class="has-children">
									<a href="#">Caste</a> 
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
												<li><a href="c_list.jsp">Caste 1</a></li>
												<li><a href="c_list.jsp">Caste 2</a></li>  
												<li><a href="c_list.jsp">Caste 3</a></li> 
												<li><a href="c_list.jsp">Caste 4</a></li> 
												<li><a href="c_list.jsp">Caste 5</a></li> 
												<li><a href="c_list.jsp">Caste 6</a></li> 
												<li><a href="c_list.jsp">Caste 7</a></li> 
												<li><a href="c_list.jsp">Caste 8</a></li> 
												<li><a href="c_list.jsp">Caste 9</a></li> 
												<li><a href="c_list.jsp">Caste 10</a></li> 
												<li><a href="c_list.jsp">Caste 11</a></li> 
												<li><a href="c_list.jsp">Caste 12</a></li> 
												<li><a href="c_list.jsp">Caste 13</a></li> 
												<li><a href="c_list.jsp">Caste 14</a></li> 
												<li><a href="c_list.jsp">Caste 15</a></li> 
												<li><a href="c_list.jsp">Caste 16</a></li> 
												<li><a href="c_list.jsp">Caste 17</a></li> 
												<li><a href="c_list.jsp">Caste 18</a></li> 
												<li><a href="c_list.jsp">Caste 19</a></li> 
												<li><a href="c_list.jsp">Caste 20</a></li> 
												<li><a href="c_list.jsp">Caste 21</a></li> 
												<li><a href="c_list.jsp">Caste 22</a></li> 
												<li><a href="c_list.jsp">Caste 23</a></li> 
												<li><a href="c_list.jsp">Caste 24</a></li> 
												<li><a href="c_list.jsp">Caste 25</a></li> 
												<li><a href="c_list.jsp">Caste 26</a></li> 
									</ul> <!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children -->
								<li class="has-children">
									<a href="products2.jsp">Religion</a> 
									<ul class="cd-secondary-dropdown is-hidden"> 
										<li class="go-back"><a href="#">Menu</a></li>
												<li><a href="r_list.jsp">Religion 1</a></li> 
												<li><a href="r_list.jsp">Religion 2</a></li> 
												<li><a href="r_list.jsp">Religion 3</a></li> 
												<li><a href="r_list.jsp">Religion 4</a></li> 
												<li><a href="r_list.jsp">Religion 5</a></li> 
												<li><a href="r_list.jsp">Religion 6</a></li> 
												<li><a href="r_list.jsp">Religion 7</a></li> 
												<li><a href="r_list.jsp">Religion 8</a></li> 
												<li><a href="r_list.jsp">Religion 9</a></li> 
												<li><a href="r_list.jsp">Religion 10</a></li> 
												<li><a href="r_list.jsp">Religion 11</a></li> 
												<li><a href="r_list.jsp">Religion 12</a></li> 
												<li><a href="r_list.jsp">Religion 13</a></li> 
												<li><a href="r_list.jsp">Religion 14</a></li> 
												<li><a href="r_list.jsp">Religion 15</a></li> 
												<li><a href="r_list.jsp">Religion 16</a></li> 
												<li><a href="r_list.jsp">Religion 17</a></li> 
									</ul><!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children --> 
								<li class="has-children">
									<a href="#">City</a> 
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
												<li><a href="city_list.jsp">City 1</a></li> 
												<li><a href="city_list.jsp">City 2</a></li> 
												<li><a href="city_list.jsp">City 3</a></li> 
												<li><a href="city_list.jsp">City 4</a></li> 
												<li><a href="city_list.jsp">City 5</a></li> 
												<li><a href="city_list.jsp">City 6</a></li> 
												<li><a href="city_list.jsp">City 7</a></li> 
												<li><a href="city_list.jsp">City 8</a></li> 
												<li><a href="city_list.jsp">City 9</a></li> 
												<li><a href="city_list.jsp">City 10</a></li> 
												<li><a href="city_list.jsp">City 11</a></li> 
												<li><a href="city_list.jsp">City 12</a></li> 
												<li><a href="city_list.jsp">City 13</a></li> 
												<li><a href="city_list.jsp">City 14</a></li> 
												<li><a href="city_list.jsp">City 15</a></li> 
												<li><a href="city_list.jsp">City 16</a></li> 
												<li><a href="city_list.jsp">City 17</a></li> 
												<li><a href="city_list.jsp">City 18</a></li> 
												<li><a href="city_list.jsp">City 19</a></li> 
												<li><a href="city_list.jsp">City 20</a></li> 
												<li><a href="city_list.jsp">City 21</a></li> 
												<li><a href="city_list.jsp">City 22</a></li> 
												<li><a href="city_list.jsp">City 23</a></li> 
												<li><a href="city_list.jsp">City 24</a></li> 
												<li><a href="city_list.jsp">City 25</a></li> 
												<li><a href="city_list.jsp">City 26</a></li> 
												<li><a href="city_list.jsp">City 27</a></li> 
												<li><a href="city_list.jsp">City 28</a></li> 
												<li><a href="city_list.jsp">City 29</a></li> 
												<li><a href="city_list.jsp">City 30</a></li> 
												<li><a href="city_list.jsp">City 31</a></li> 
												<li><a href="city_list.jsp">City 32</a></li> 
												<li><a href="city_list.jsp">City 33</a></li> 
												<li><a href="city_list.jsp">City 34</a></li> 
												<li><a href="city_list.jsp">City 35</a></li> 
												<li><a href="city_list.jsp">City 36</a></li> 
									</ul><!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children -->  
								<li class="has-children">
									<a href="#">Occupation</a>
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
												<li><a href="o_list.jsp">IT Software </a></li> 
												<li><a href="o_list.jsp">Teacher  </a></li>
												<li><a href="o_list.jsp">Business man </a></li>
												<li><a href="o_list.jsp">Lawyers</a></li>
												<li><a href="o_list.jsp">Defence </a></li>
												<li><a href="o_list.jsp">IAS </a></li>
												<li><a href="o_list.jsp">Govt.Services </a></li>
												<li><a href="o_list.jsp">Doctors </a></li>
									</ul><!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children -->  
								<li class="has-children">
									<a href="#">State</a>
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
												<li><a href="s_list.jsp">State 1</a></li> 
												<li><a href="s_list.jsp">State 2</a></li> 
												<li><a href="s_list.jsp">State 3</a></li> 
												<li><a href="s_list.jsp">State 4</a></li> 
												<li><a href="s_list.jsp">State 5</a></li> 
												<li><a href="s_list.jsp">State 6</a></li> 
												<li><a href="s_list.jsp">State 7</a></li> 
												<li><a href="s_list.jsp">State 8</a></li> 
												<li><a href="s_list.jsp">State 9</a></li> 
												<li><a href="s_list.jsp">State 10</a></li> 
												<li><a href="s_list.jsp">State 11</a></li> 
												<li><a href="s_list.jsp">State 12</a></li> 
												<li><a href="s_list.jsp">State 13</a></li> 
												<li><a href="s_list.jsp">State 14</a></li> 
												<li><a href="s_list.jsp">State 15</a></li> 
												<li><a href="s_list.jsp">State 16</a></li> 
												<li><a href="s_list.jsp">State 17</a></li> 
												<li><a href="s_list.jsp">State 18</a></li> 
												<li><a href="s_list.jsp">State 19</a></li> 
												<li><a href="s_list.jsp">State 20</a></li> 
												<li><a href="s_list.jsp">State 21</a></li> 
												<li><a href="s_list.jsp">State 22</a></li> 
												<li><a href="s_list.jsp">State 23</a></li> 
												<li><a href="s_list.jsp">State 24</a></li> 
												<li><a href="s_list.jsp">State 25</a></li> 
												<li><a href="s_list.jsp">State 26</a></li> 
												<li><a href="s_list.jsp">State 27</a></li> 
												<li><a href="s_list.jsp">State 28</a></li> 
												<li><a href="s_list.jsp">State 29</a></li> 
												<li><a href="s_list.jsp">State 30</a></li> 
												<li><a href="s_list.jsp">State 31</a></li> 
												<li><a href="s_list.jsp">State 32</a></li> 
												<li><a href="s_list.jsp">State 33</a></li> 
												<li><a href="s_list.jsp">State 34</a></li> 
												<li><a href="s_list.jsp">State 35</a></li> 
												<li><a href="s_list.jsp">State 36</a></li> 
												<li><a href="s_list.jsp">State 37</a></li> 
												<li><a href="s_list.jsp">State 38</a></li> 
												<li><a href="s_list.jsp">State 39</a></li> 
												<li><a href="s_list.jsp">State 40</a></li> 
												<li><a href="s_list.jsp">State 41</a></li> 
												<li><a href="s_list.jsp">State 42</a></li> 
												<li><a href="s_list.jsp">State 43</a></li> 
												<li><a href="s_list.jsp">State 44</a></li> 
												<li><a href="s_list.jsp">State 45</a></li> 
									</ul><!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children -->  
								<li class="has-children">
									<a href="#">NRI</a>
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
												<li><a href="nri_list.jsp">Country 1</a></li> 
												<li><a href="nri_list.jsp">Country 2</a></li> 
												<li><a href="nri_list.jsp">Country 3</a></li> 
												<li><a href="nri_list.jsp">Country 4</a></li> 
												<li><a href="nri_list.jsp">Country 5</a></li> 
												<li><a href="nri_list.jsp">Country 6</a></li> 
												<li><a href="nri_list.jsp">Country 7</a></li> 
												<li><a href="nri_list.jsp">Country 8</a></li> 
												<li><a href="nri_list.jsp">Country 9</a></li> 
												<li><a href="nri_list.jsp">Country 10</a></li> 
												<li><a href="nri_list.jsp">Country 11</a></li> 
												<li><a href="nri_list.jsp">Country 12</a></li> 
									</ul><!-- .cd-secondary-dropdown --> 
								</li>  
							</ul> <!-- .cd-dropdown-content -->
						</nav> <!-- .cd-dropdown -->
					</div> <!-- .cd-dropdown-wrapper -->	 
				</div>
           <div class="pull-right">
          	<nav class="navbar nav_bottom" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
		  <div class="navbar-header nav_2">
		      <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">Menu
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		   </div> 
		   <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
		        <ul class="nav navbar-nav nav_1">
		            <li><a href="index.jsp">Home</a></li>
		            <li><a href="about.jsp">About</a></li>
		            <li><a href="search.jsp">Search</a></li>
		            <li><a href="app.jsp" target="_blank">Mobile</a></li>
					  <!--<li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Search<span class="caret"></span></a>
		              <ul class="dropdown-menu" role="menu">
		                <li><a href="search.jsp">Regular Search</a></li>
		                <li><a href="profile.jsp">Recently Viewed Profiles</a></li>
		                <li><a href="search-id.jsp">Search By Profile ID</a></li>
		                <li><a href="faq.jsp">Faq</a></li>
		                <li><a href="shortcodes.jsp">Shortcodes</a></li>
		              </ul>
		            </li>-->
		            <li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Quick Search<span class="caret"></span></a>
		              <ul class="dropdown-menu" role="menu">
		                <div class="banner-bottom-login">
		<div class="w3agile_banner_btom_login">
			<form action="#" method="post">
				<div class="w3agile__text w3agile_banner_btom_login_left">
					<h4>I'm looking for a</h4>
					<select id="country" onchange="change_country(this.value)" class="frm-field required">
						<option value="null">Bride</option>
						<option value="null">Groom</option>   
					</select>
				</div>
				<div class="w3agile__text w3agile_banner_btom_login_left1">
					<h4>Aged</h4>
					<select id="country1" onchange="change_country(this.value)" class="frm-field required">
						<option value="null">20</option>
						<option value="null">21</option>   
						<option value="null">22</option>   
						<option value="null">23</option>   
						<option value="null">24</option>   
						<option value="null">25</option>  
						<option value="null">- - -</option>   					
					</select>
					<span>To </span>
					<select id="country2" onchange="change_country(this.value)" class="frm-field required">
						<option value="null">30</option>
						<option value="null">31</option>   
						<option value="null">32</option>   
						<option value="null">33</option>   
						<option value="null">34</option>   
						<option value="null">35</option>  
						<option value="null">- - -</option>   					
					</select>
				</div>
				<div class="w3agile__text w3agile_banner_btom_login_left2">
					<h4>Religion</h4>
					<select id="country3" onchange="change_country(this.value)" class="frm-field required">
						<option value="null">Hindu</option>  
						<option value="null">Muslim</option>   
						<option value="null">Christian</option>   
						<option value="null">Sikh</option>   
						<option value="null">Jain</option>   
						<option value="null">Buddhist</option>
						<option value="null">No Religious Belief</option>   					
					</select>
				</div>
				<div class="w3agile_banner_btom_login_left3">
					<input type="submit" value="Search" />
				</div>
				<div class="clearfix"> </div>
			</form>
		</div>
	</div>
		              </ul>
		            </li>
		            <li class="last"><a href="contact.jsp">Contacts</a></li>
		        </ul>
		     </div><!-- /.navbar-collapse -->
		    </nav>
		   </div> <!-- end pull-right -->
          <div class="clearfix"> </div>
        </div> <!-- end container -->
      </div> <!-- end navbar-inner -->
    </div> <!-- end navbar-inverse-blue -->
<!--  Navigation End -->
</header>
<!-- /header -->

	<!-- inner banner -->	
	<div class="w3layouts-inner-banner">
	<div class="container">
		<div class="logo">
			<h1><a class="cd-logo link link--takiri" href="index.jsp">Match <span><i class="fa fa-heart" aria-hidden="true"></i>Made in heaven.</span></a></h1>
		</div>
		<div class="clearfix"></div>
		</div>
	</div>
	<!-- //inner banner -->
	
	<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs"><a href="index.jsp">Home</a> > <span>Write to Us</span></span>
		</div>
	</div>
	<!-- //breadcrumbs -->
	
	<!-- Write to us -->
	<div class="write-agileits">
		<div class="container">
			<h2>Write to Us</h2>
			<form action="#" method="post">
			<div class="agileits">
				<label>Email:</label>
				<input type="email" name="email" placeholder="" required="required" />
			</div>
			<div class="agileits">
				<label>Name:</label>
				<input type="text" name="name" placeholder="" />
			</div>
			<div class="agileits">
				<label>Type of issue:</label>
				<select id="" class="frm-field required">
					<option value="select">--Select Issue--</option>  
					<option value="Category">Issue1</option>   
					<option value="Category">Issue2</option>   
					<option value="Category">Issue3</option>   
					<option value="Category">Issue4</option>   
					<option value="Category">Issue5</option>
					<option value="Category">Issue6</option>   					
				</select>
			</div>
			<div class="agileits w3_writetoustextarea">
				<label>Description:</label>
				<textarea name="Comments" placeholder=""></textarea>
			</div>
			<div class="agileits w3_attachments">
				<label>Attachments:</label>
				<div class="photos-upload-view">
						<form id="upload" action="index.jsp" method="POST" enctype="multipart/form-data">

						<input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="300000" />

						<div>
							<input type="file" id="fileselect" name="fileselect[]" multiple="multiple" />
							<div id="filedrag">or drop files here</div>
						</div>

						<div id="submitbutton">
							<button type="submit">Upload Files</button>
						</div>


						<div id="messages">
						<p>Status Messages</p>
						</div>
						</div>
					
				</div>
				<div class="w3_submit">
					<input type="submit" value="Submit"/>
				</div>
			</form>
			</form>
		</div>
	</div>
	<!-- //Write to us -->
		
	<!-- browse profiles -->
	<div class="w3layouts-browse text-center">
		<div class="container">
			<h3>Browse Matchmaking Profiles by</h3>
			<div class="col-md-4 w3-browse-grid">
				<h4>By Country</h4>
				<ul>
					<li><a href="nri_list.jsp">Country 1</a></li>
					<li><a href="nri_list.jsp">Country 2</a></li>
					<li><a href="nri_list.jsp">Country 3</a></li>
					<li><a href="nri_list.jsp">Country 4</a></li>
					<li><a href="nri_list.jsp">Country 5</a></li>
					<li><a href="nri_list.jsp">Country 6</a></li>
					<li><a href="nri_list.jsp">Country 7</a></li>
					<li><a href="nri_list.jsp">Country 8</a></li>
					<li><a href="nri_list.jsp">Country 9</a></li>
					<li><a href="nri_list.jsp">Country 10</a></li>
					<li><a href="nri_list.jsp">Country 11</a></li>
					<li class="more"><a href="nri_list.jsp">more..</a></li>
				</ul>
			</div>
			<div class="col-md-4 w3-browse-grid">
				<h4>By Religion</h4>
				<ul>
					<li><a href="r_list.jsp">Religion 1</a></li>
					<li><a href="r_list.jsp">Religion 2</a></li>
					<li><a href="r_list.jsp">Religion 3</a></li>
					<li><a href="r_list.jsp">Religion 4</a></li>
					<li><a href="r_list.jsp">Religion 5</a></li>
					<li><a href="r_list.jsp">Religion 6</a></li>
					<li><a href="r_list.jsp">Religion 7</a></li>
					<li><a href="r_list.jsp">Religion 8</a></li>
					<li><a href="r_list.jsp">Religion 9</a></li>
					<li><a href="r_list.jsp">Religion 10</a></li>
					<li><a href="r_list.jsp">Religion 11</a></li>
					<li class="more"><a href="r_list.jsp">more..</a></li>
				</ul>
			</div>
			<div class="col-md-4 w3-browse-grid">
				<h4>By Community</h4>
				<ul>
					<li><a href="r_list.jsp">Community 1</a></li>
					<li><a href="r_list.jsp">Community 2</a></li>
					<li><a href="r_list.jsp">Community 3</a></li>
					<li><a href="r_list.jsp">Community 4</a></li>
					<li><a href="r_list.jsp">Community 5</a></li>
					<li><a href="r_list.jsp">Community 6</a></li>
					<li><a href="r_list.jsp">Community 7</a></li>
					<li><a href="r_list.jsp">Community 8</a></li>
					<li><a href="r_list.jsp">Community 9</a></li>
					<li class="more"><a href="r_list.jsp">more..</a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //browse profiles -->

	<!-- Get started -->
	<div class="w3layous-story text-center">
		<div class="container">
			<h4>Your story is waiting to happen!  <a href="index.jsp">Get started</a></h4>
		</div>
	</div>
	<!-- //Get started -->
	
	
<!-- footer -->
<footer>
	<div class="footer">
		<div class="container">
			<div class="footer-info w3-agileits-info">
				<div class="col-md-4 address-left agileinfo">
					<div class="footer-logo header-logo">
						<h6>Get in Touch.</h6>
					</div>
					<ul>
						<li><i class="fa fa-map-marker"></i> 123 San Sebastian, New York City USA.</li>
						<li><i class="fa fa-mobile"></i> 333 222 3333 </li>
						<li><i class="fa fa-phone"></i> +222 11 4444 </li>
						<li><i class="fa fa-envelope-o"></i> <a href="mailto:example@mail.com"> mail@example.com</a></li>
					</ul> 
				</div>
				<div class="col-md-8 address-right">
					<div class="col-md-4 footer-grids">
						<h3>Company</h3>
						<ul>
							<li><a href="about.jsp">About Us</a></li>
							<li><a href="feedback.jsp">Feedback</a></li>  
							<li><a href="help.jsp">Help</a></li>  
							<li><a href="tips.jsp">Safety Tips</a></li>
							<li><a href="typo.jsp">Typography</a></li>
							<li><a href="icons.jsp">Icons Page</a></li>
						</ul>
					</div>
					<div class="col-md-4 footer-grids">
						<h3>Quick links</h3>
						<ul>
							<li><a href="terms.jsp">Terms of use</a></li>
							<li><a href="privacy_policy.jsp">Privacy Policy</a></li>
							<li><a href="contact.jsp">Contact Us</a></li>
							<li><a href="faq.jsp">FAQ</a></li>
							<li><a href="sitemap.jsp">Sitemap</a></li>
						</ul> 
					</div>
					<div class="col-md-4 footer-grids">
						<h3>Follow Us on</h3>
						<section class="social">
                        <ul>
							<li><a class="icon fb" href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a class="icon tw" href="#"><i class="fa fa-twitter"></i></a></li>	
							<li><a class="icon gp" href="#"><i class="fa fa-google-plus"></i></a></li>
						</ul>
						</section>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>	
	<div class="copy-right"> 
		<div class="container">
			<p>© 2017 Match. All rights reserved | Design by <a href="http://w3layouts.com"> W3layouts.</a></p>
		</div>
	</div> 
</footer>
<!-- //footer -->	
<!-- menu js aim -->
	<script src="js/jquery.menu-aim.js"> </script>
	<script src="js/main.js"></script> <!-- Resource jQuery -->
	<!-- //menu js aim -->
	<!-- drag and drop js -->
	<script src="js/filedrag.js"></script>
	<!-- //drag and drop js -->
	<!-- for bootstrap working -->
		<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
	  			containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
	 		};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
							
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!-- for smooth scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
	</script>
	<!-- //for smooth scrolling -->
</body>
<!-- //body -->
</html>
<!-- //html -->
