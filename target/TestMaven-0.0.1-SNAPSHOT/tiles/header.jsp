<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- head -->
<head>
<title>Match a Matrimonial Category Bootstrap Responsive Web
	Template | index :: w3layouts</title>
<link href="/css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- bootstrap-CSS -->
<link href="/css/font-awesome.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- Fontawesome-CSS -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type='text/javascript' src='/js/jquery-2.2.3.min.js'></script>
<!-- Custom Theme files -->
<link href="/css/menu.css" rel="stylesheet" type="text/css" media="all" />
<!-- menu style -->
<!--theme-style-->
<link href="/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<link rel="stylesheet" type="text/css"
	href="/css/easy-responsive-tabs.css " />
<!--meta data-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords"
	content="Match Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!--//meta data-->
<!-- online fonts -->
<link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&amp;subset=devanagari,latin-ext"
	rel="stylesheet">
<link
	href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	rel="stylesheet">
<!-- /online fonts -->
<!-- nav smooth scroll -->
<script>
	$(document).ready(function() {
		$(".dropdown").hover(function() {
			$('.dropdown-menu', this).stop(true, true).slideDown("fast");
			$(this).toggleClass('open');
		}, function() {
			$('.dropdown-menu', this).stop(true, true).slideUp("fast");
			$(this).toggleClass('open');
		});
	});
</script>
<!-- //nav smooth scroll -->
<!-- Calendar -->
<link rel="stylesheet" href="/css/jquery-ui.css" />
<script src="/js/jquery-ui.js"></script>
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
<!-- //Calendar -->
<link rel="stylesheet" href="/css/intlTelInput.css">
</head>
<body>
	<!-- header -->
	<header> <!--  Navigation Start -->
	<div class="navbar navbar-inverse-blue navbar">
		<!--<div class="navbar navbar-inverse-blue navbar-fixed-top">-->
		<div class="navbar-inner">
			<div class="container">
				<div class="menu">
					<div class="cd-dropdown-wrapper">
						<a class="cd-dropdown-trigger" href="#0">Browse Profiles by</a>
						<nav class="cd-dropdown"> <a href="#0" class="cd-close">Close</a>
						<ul class="cd-dropdown-content">
							<li><a href="matches.jsp">All Profiles</a></li>
							<li class="has-children"><a href="#">Mother Tongue</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="l_list.jsp">Language 1</a></li>
									<li><a href="l_list.jsp">Language 2</a></li>
									<li><a href="l_list.jsp">Language 3</a></li>
									<li><a href="l_list.jsp">Language 4</a></li>
									<li><a href="l_list.jsp">Language 5</a></li>
									<li><a href="l_list.jsp">Language 6</a></li>
									<li><a href="l_list.jsp">Language 7</a></li>
									<li><a href="l_list.jsp">Language 8</a></li>
									<li><a href="l_list.jsp">Language 9</a></li>
									<li><a href="l_list.jsp">Language 10</a></li>
									<li><a href="l_list.jsp">Language 11</a></li>
									<li><a href="l_list.jsp">Language 12</a></li>
									<li><a href="l_list.jsp">Language 13</a></li>
									<li><a href="l_list.jsp">Language 14</a></li>
									<li><a href="l_list.jsp">Language 15</a></li>
									<li><a href="l_list.jsp">Language 16</a></li>
									<li><a href="l_list.jsp">Language 17</a></li>
									<li><a href="l_list.jsp">Language 18</a></li>
									<li><a href="l_list.jsp">Language 19</a></li>
									<li><a href="l_list.jsp">Language 20</a></li>

								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="#">Caste</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="c_list.jsp">Caste 1</a></li>
									<li><a href="c_list.jsp">Caste 2</a></li>
									<li><a href="c_list.jsp">Caste 3</a></li>
									<li><a href="c_list.jsp">Caste 4</a></li>
									<li><a href="c_list.jsp">Caste 5</a></li>
									<li><a href="c_list.jsp">Caste 6</a></li>
									<li><a href="c_list.jsp">Caste 7</a></li>
									<li><a href="c_list.jsp">Caste 8</a></li>
									<li><a href="c_list.jsp">Caste 9</a></li>
									<li><a href="c_list.jsp">Caste 10</a></li>
									<li><a href="c_list.jsp">Caste 11</a></li>
									<li><a href="c_list.jsp">Caste 12</a></li>
									<li><a href="c_list.jsp">Caste 13</a></li>
									<li><a href="c_list.jsp">Caste 14</a></li>
									<li><a href="c_list.jsp">Caste 15</a></li>
									<li><a href="c_list.jsp">Caste 16</a></li>
									<li><a href="c_list.jsp">Caste 17</a></li>
									<li><a href="c_list.jsp">Caste 18</a></li>
									<li><a href="c_list.jsp">Caste 19</a></li>
									<li><a href="c_list.jsp">Caste 20</a></li>
									<li><a href="c_list.jsp">Caste 21</a></li>
									<li><a href="c_list.jsp">Caste 22</a></li>
									<li><a href="c_list.jsp">Caste 23</a></li>
									<li><a href="c_list.jsp">Caste 24</a></li>
									<li><a href="c_list.jsp">Caste 25</a></li>
									<li><a href="c_list.jsp">Caste 26</a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="products2.jsp">Religion</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="r_list.jsp">Religion 1</a></li>
									<li><a href="r_list.jsp">Religion 2</a></li>
									<li><a href="r_list.jsp">Religion 3</a></li>
									<li><a href="r_list.jsp">Religion 4</a></li>
									<li><a href="r_list.jsp">Religion 5</a></li>
									<li><a href="r_list.jsp">Religion 6</a></li>
									<li><a href="r_list.jsp">Religion 7</a></li>
									<li><a href="r_list.jsp">Religion 8</a></li>
									<li><a href="r_list.jsp">Religion 9</a></li>
									<li><a href="r_list.jsp">Religion 10</a></li>
									<li><a href="r_list.jsp">Religion 11</a></li>
									<li><a href="r_list.jsp">Religion 12</a></li>
									<li><a href="r_list.jsp">Religion 13</a></li>
									<li><a href="r_list.jsp">Religion 14</a></li>
									<li><a href="r_list.jsp">Religion 15</a></li>
									<li><a href="r_list.jsp">Religion 16</a></li>
									<li><a href="r_list.jsp">Religion 17</a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="#">City</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="city_list.jsp">City 1</a></li>
									<li><a href="city_list.jsp">City 2</a></li>
									<li><a href="city_list.jsp">City 3</a></li>
									<li><a href="city_list.jsp">City 4</a></li>
									<li><a href="city_list.jsp">City 5</a></li>
									<li><a href="city_list.jsp">City 6</a></li>
									<li><a href="city_list.jsp">City 7</a></li>
									<li><a href="city_list.jsp">City 8</a></li>
									<li><a href="city_list.jsp">City 9</a></li>
									<li><a href="city_list.jsp">City 10</a></li>
									<li><a href="city_list.jsp">City 11</a></li>
									<li><a href="city_list.jsp">City 12</a></li>
									<li><a href="city_list.jsp">City 13</a></li>
									<li><a href="city_list.jsp">City 14</a></li>
									<li><a href="city_list.jsp">City 15</a></li>
									<li><a href="city_list.jsp">City 16</a></li>
									<li><a href="city_list.jsp">City 17</a></li>
									<li><a href="city_list.jsp">City 18</a></li>
									<li><a href="city_list.jsp">City 19</a></li>
									<li><a href="city_list.jsp">City 20</a></li>
									<li><a href="city_list.jsp">City 21</a></li>
									<li><a href="city_list.jsp">City 22</a></li>
									<li><a href="city_list.jsp">City 23</a></li>
									<li><a href="city_list.jsp">City 24</a></li>
									<li><a href="city_list.jsp">City 25</a></li>
									<li><a href="city_list.jsp">City 26</a></li>
									<li><a href="city_list.jsp">City 27</a></li>
									<li><a href="city_list.jsp">City 28</a></li>
									<li><a href="city_list.jsp">City 29</a></li>
									<li><a href="city_list.jsp">City 30</a></li>
									<li><a href="city_list.jsp">City 31</a></li>
									<li><a href="city_list.jsp">City 32</a></li>
									<li><a href="city_list.jsp">City 33</a></li>
									<li><a href="city_list.jsp">City 34</a></li>
									<li><a href="city_list.jsp">City 35</a></li>
									<li><a href="city_list.jsp">City 36</a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="#">Occupation</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="o_list.jsp">IT Software </a></li>
									<li><a href="o_list.jsp">Teacher </a></li>
									<li><a href="o_list.jsp">Business man </a></li>
									<li><a href="o_list.jsp">Lawyers</a></li>
									<li><a href="o_list.jsp">Defence </a></li>
									<li><a href="o_list.jsp">IAS </a></li>
									<li><a href="o_list.jsp">Govt.Services </a></li>
									<li><a href="o_list.jsp">Doctors </a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="#">State</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="s_list.jsp">State 1</a></li>
									<li><a href="s_list.jsp">State 2</a></li>
									<li><a href="s_list.jsp">State 3</a></li>
									<li><a href="s_list.jsp">State 4</a></li>
									<li><a href="s_list.jsp">State 5</a></li>
									<li><a href="s_list.jsp">State 6</a></li>
									<li><a href="s_list.jsp">State 7</a></li>
									<li><a href="s_list.jsp">State 8</a></li>
									<li><a href="s_list.jsp">State 9</a></li>
									<li><a href="s_list.jsp">State 10</a></li>
									<li><a href="s_list.jsp">State 11</a></li>
									<li><a href="s_list.jsp">State 12</a></li>
									<li><a href="s_list.jsp">State 13</a></li>
									<li><a href="s_list.jsp">State 14</a></li>
									<li><a href="s_list.jsp">State 15</a></li>
									<li><a href="s_list.jsp">State 16</a></li>
									<li><a href="s_list.jsp">State 17</a></li>
									<li><a href="s_list.jsp">State 18</a></li>
									<li><a href="s_list.jsp">State 19</a></li>
									<li><a href="s_list.jsp">State 20</a></li>
									<li><a href="s_list.jsp">State 21</a></li>
									<li><a href="s_list.jsp">State 22</a></li>
									<li><a href="s_list.jsp">State 23</a></li>
									<li><a href="s_list.jsp">State 24</a></li>
									<li><a href="s_list.jsp">State 25</a></li>
									<li><a href="s_list.jsp">State 26</a></li>
									<li><a href="s_list.jsp">State 27</a></li>
									<li><a href="s_list.jsp">State 28</a></li>
									<li><a href="s_list.jsp">State 29</a></li>
									<li><a href="s_list.jsp">State 30</a></li>
									<li><a href="s_list.jsp">State 31</a></li>
									<li><a href="s_list.jsp">State 32</a></li>
									<li><a href="s_list.jsp">State 33</a></li>
									<li><a href="s_list.jsp">State 34</a></li>
									<li><a href="s_list.jsp">State 35</a></li>
									<li><a href="s_list.jsp">State 36</a></li>
									<li><a href="s_list.jsp">State 37</a></li>
									<li><a href="s_list.jsp">State 38</a></li>
									<li><a href="s_list.jsp">State 39</a></li>
									<li><a href="s_list.jsp">State 40</a></li>
									<li><a href="s_list.jsp">State 41</a></li>
									<li><a href="s_list.jsp">State 42</a></li>
									<li><a href="s_list.jsp">State 43</a></li>
									<li><a href="s_list.jsp">State 44</a></li>
									<li><a href="s_list.jsp">State 45</a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="#">NRI</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="nri_list.jsp">Country 1</a></li>
									<li><a href="nri_list.jsp">Country 2</a></li>
									<li><a href="nri_list.jsp">Country 3</a></li>
									<li><a href="nri_list.jsp">Country 4</a></li>
									<li><a href="nri_list.jsp">Country 5</a></li>
									<li><a href="nri_list.jsp">Country 6</a></li>
									<li><a href="nri_list.jsp">Country 7</a></li>
									<li><a href="nri_list.jsp">Country 8</a></li>
									<li><a href="nri_list.jsp">Country 9</a></li>
									<li><a href="nri_list.jsp">Country 10</a></li>
									<li><a href="nri_list.jsp">Country 11</a></li>
									<li><a href="nri_list.jsp">Country 12</a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
						</ul>
						<!-- .cd-dropdown-content --> </nav>
						<!-- .cd-dropdown -->
					</div>
					<!-- .cd-dropdown-wrapper -->
				</div>
				<div class="pull-right">
					<nav class="navbar nav_bottom" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header nav_2">
						<button type="button"
							class="navbar-toggle collapsed navbar-toggle1"
							data-toggle="collapse" data-target="#bs-megadropdown-tabs">
							Menu <span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
						<ul class="nav navbar-nav nav_1">
							<li class="active"><a href="index.jsp">Home</a></li>
							<li><a href="about.jsp">About</a></li>
							<li><a href="search.jsp">Search</a></li>
							<li><a href="app.jsp" target="_blank">Mobile</a></li>
							<!--<li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Search<span class="caret"></span></a>
		              <ul class="dropdown-menu" role="menu">
		                <li><a href="search.jsp">Regular Search</a></li>
		                <li><a href="profile.jsp">Recently Viewed Profiles</a></li>
		                <li><a href="search-id.jsp">Search By Profile ID</a></li>
		                <li><a href="faq.jsp">Faq</a></li>
		                <li><a href="shortcodes.jsp">Shortcodes</a></li>
		              </ul>
		            </li>-->
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown">Quick Search<span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<div class="banner-bottom-login">
										<div class="w3agile_banner_btom_login">
											<form action="#" method="post">
												<div class="w3agile__text w3agile_banner_btom_login_left">
													<h4>I'm looking for a</h4>
													<select id="country" onchange="change_country(this.value)"
														class="frm-field required">
														<option value="null">Bride</option>
														<option value="null">Groom</option>
													</select>
												</div>
												<div class="w3agile__text w3agile_banner_btom_login_left1">
													<h4>Aged</h4>
													<select id="country1" onchange="change_country(this.value)"
														class="frm-field required">
														<option value="null">20</option>
														<option value="null">21</option>
														<option value="null">22</option>
														<option value="null">23</option>
														<option value="null">24</option>
														<option value="null">25</option>
														<option value="null">- - -</option>
													</select> <span>To </span> <select id="country2"
														onchange="change_country(this.value)"
														class="frm-field required">
														<option value="null">30</option>
														<option value="null">31</option>
														<option value="null">32</option>
														<option value="null">33</option>
														<option value="null">34</option>
														<option value="null">35</option>
														<option value="null">- - -</option>
													</select>
												</div>
												<div class="w3agile__text w3agile_banner_btom_login_left2">
													<h4>Religion</h4>
													<select id="country3" onchange="change_country(this.value)"
														class="frm-field required">
														<option value="null">Hindu</option>
														<option value="null">Muslim</option>
														<option value="null">Christian</option>
														<option value="null">Sikh</option>
														<option value="null">Jain</option>
														<option value="null">Buddhist</option>
														<option value="null">No Religious Belief</option>
													</select>
												</div>
												<div class="w3agile_banner_btom_login_left3">
													<input type="submit" value="Search" />
												</div>
												<div class="clearfix"></div>
											</form>
										</div>
									</div>
								</ul></li>
							<li class="last"><a href="contact.jsp">Contacts</a></li>
						</ul>
					</div>
					<!-- /.navbar-collapse --> </nav>
				</div>
				<!-- end pull-right -->
				<div class="clearfix"></div>
			</div>
			<!-- end container -->
		</div>
		<!-- end navbar-inner -->
	</div>
	<!-- end navbar-inverse-blue --> <!-- ============================  Navigation End ============================ -->
	</header>
	<!-- /header -->

</body>
</html>