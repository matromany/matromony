<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MatriMonial.com</title>
</head>
<body style="width: auto;">

	<table border="1" cellspacing="0" cellpadding="0" style="width:100%">
	<tr align="center">
	<td colspan="1" style="width: 100%;"><tiles:insertAttribute name="header"/></td>
	
	</tr>
	<tr align="center">
	<td colspan="2" style="width: 100%;"><tiles:insertAttribute name="menu"/></td>
	
	</tr>
	
	<tr align="center">
	
	<td style="width: 100%;"><tiles:insertAttribute name="body"/></td>
	</tr>
	
	<tr align="center"> 
	<td colspan="2" style="width: 100%;"><tiles:insertAttribute name="footer"/></td>
	</tr>
	
	</table>

</body>
</html>