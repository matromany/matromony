<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<title>Match a Matrimonial Category Bootstrap Responsive Web
	Template | index :: w3layouts</title>

<!--meta data-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords"
	content="Match Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!--//meta data-->
<!-- online fonts -->
<link href="http://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&amp;subset=devanagari,latin-ext"
	rel="stylesheet">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	rel="stylesheet">
<!-- /online fonts -->
<!-- nav smooth scroll -->
<script>
	$(document).ready(function() {
		$(".dropdown").hover(function() {
			$('.dropdown-menu', this).stop(true, true).slideDown("fast");
			$(this).toggleClass('open');
		}, function() {
			$('.dropdown-menu', this).stop(true, true).slideUp("fast");
			$(this).toggleClass('open');
		});
	});
</script>
<!-- //nav smooth scroll -->
<!-- Calendar -->
<script src="/WEB-INF/resources/js/jquery-ui.js"></script>
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
<!-- //Calendar -->

</head>
<body>
<header> <!--  Navigation Start -->
	<div class="navbar navbar-inverse-blue navbar">
		<!--<div class="navbar navbar-inverse-blue navbar-fixed-top">-->
		<div class="navbar-inner">
			<div class="container">
				<div class="menu">
					<div class="cd-dropdown-wrapper">
						<a class="cd-dropdown-trigger" href="#0">Browse Profiles by</a>
						<nav class="cd-dropdown"> <a href="#0" class="cd-close">Close</a>
						<ul class="cd-dropdown-content">
							<li><a href="matches">All Profiles</a></li>
							<li class="has-children"><a href="#">Mother Tongue</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="l_list">Language 1</a></li>
									<li><a href="l_list">Language 2</a></li>
									<li><a href="l_list">Language 3</a></li>
									<li><a href="l_list">Language 4</a></li>
									<li><a href="l_list">Language 5</a></li>
									<li><a href="l_list">Language 6</a></li>
									<li><a href="l_list">Language 7</a></li>
									<li><a href="l_list">Language 8</a></li>
									<li><a href="l_list">Language 9</a></li>
									<li><a href="l_list">Language 10</a></li>
									<li><a href="l_list">Language 11</a></li>
									<li><a href="l_list">Language 12</a></li>
									<li><a href="l_list">Language 13</a></li>
									<li><a href="l_list">Language 14</a></li>
									<li><a href="l_list">Language 15</a></li>
									<li><a href="l_list">Language 16</a></li>
									<li><a href="l_list">Language 17</a></li>
									<li><a href="l_list">Language 18</a></li>
									<li><a href="l_list">Language 19</a></li>
									<li><a href="l_list">Language 20</a></li>

								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="#">Caste</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="c_list">Caste 1</a></li>
									<li><a href="c_list">Caste 2</a></li>
									<li><a href="c_list">Caste 3</a></li>
									<li><a href="c_list">Caste 4</a></li>
									<li><a href="c_list">Caste 5</a></li>
									<li><a href="c_list">Caste 6</a></li>
									<li><a href="c_list">Caste 7</a></li>
									<li><a href="c_list">Caste 8</a></li>
									<li><a href="c_list">Caste 9</a></li>
									<li><a href="c_list">Caste 10</a></li>
									<li><a href="c_list">Caste 11</a></li>
									<li><a href="c_list">Caste 12</a></li>
									<li><a href="c_list">Caste 13</a></li>
									<li><a href="c_list">Caste 14</a></li>
									<li><a href="c_list">Caste 15</a></li>
									<li><a href="c_list">Caste 16</a></li>
									<li><a href="c_list">Caste 17</a></li>
									<li><a href="c_list">Caste 18</a></li>
									<li><a href="c_list">Caste 19</a></li>
									<li><a href="c_list">Caste 20</a></li>
									<li><a href="c_list">Caste 21</a></li>
									<li><a href="c_list">Caste 22</a></li>
									<li><a href="c_list">Caste 23</a></li>
									<li><a href="c_list">Caste 24</a></li>
									<li><a href="c_list">Caste 25</a></li>
									<li><a href="c_list">Caste 26</a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="products2">Religion</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="r_list">Religion 1</a></li>
									<li><a href="r_list">Religion 2</a></li>
									<li><a href="r_list">Religion 3</a></li>
									<li><a href="r_list">Religion 4</a></li>
									<li><a href="r_list">Religion 5</a></li>
									<li><a href="r_list">Religion 6</a></li>
									<li><a href="r_list">Religion 7</a></li>
									<li><a href="r_list">Religion 8</a></li>
									<li><a href="r_list">Religion 9</a></li>
									<li><a href="r_list">Religion 10</a></li>
									<li><a href="r_list">Religion 11</a></li>
									<li><a href="r_list">Religion 12</a></li>
									<li><a href="r_list">Religion 13</a></li>
									<li><a href="r_list">Religion 14</a></li>
									<li><a href="r_list">Religion 15</a></li>
									<li><a href="r_list">Religion 16</a></li>
									<li><a href="r_list">Religion 17</a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="#">City</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="city_list">City 1</a></li>
									<li><a href="city_list">City 2</a></li>
									<li><a href="city_list">City 3</a></li>
									<li><a href="city_list">City 4</a></li>
									<li><a href="city_list">City 5</a></li>
									<li><a href="city_list">City 6</a></li>
									<li><a href="city_list">City 7</a></li>
									<li><a href="city_list">City 8</a></li>
									<li><a href="city_list">City 9</a></li>
									<li><a href="city_list">City 10</a></li>
									<li><a href="city_list">City 11</a></li>
									<li><a href="city_list">City 12</a></li>
									<li><a href="city_list">City 13</a></li>
									<li><a href="city_list">City 14</a></li>
									<li><a href="city_list">City 15</a></li>
									<li><a href="city_list">City 16</a></li>
									<li><a href="city_list">City 17</a></li>
									<li><a href="city_list">City 18</a></li>
									<li><a href="city_list">City 19</a></li>
									<li><a href="city_list">City 20</a></li>
									<li><a href="city_list">City 21</a></li>
									<li><a href="city_list">City 22</a></li>
									<li><a href="city_list">City 23</a></li>
									<li><a href="city_list">City 24</a></li>
									<li><a href="city_list">City 25</a></li>
									<li><a href="city_list">City 26</a></li>
									<li><a href="city_list">City 27</a></li>
									<li><a href="city_list">City 28</a></li>
									<li><a href="city_list">City 29</a></li>
									<li><a href="city_list">City 30</a></li>
									<li><a href="city_list">City 31</a></li>
									<li><a href="city_list">City 32</a></li>
									<li><a href="city_list">City 33</a></li>
									<li><a href="city_list">City 34</a></li>
									<li><a href="city_list">City 35</a></li>
									<li><a href="city_list">City 36</a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="#">Occupation</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="o_list">IT Software </a></li>
									<li><a href="o_list">Teacher </a></li>
									<li><a href="o_list">Business man </a></li>
									<li><a href="o_list">Lawyers</a></li>
									<li><a href="o_list">Defence </a></li>
									<li><a href="o_list">IAS </a></li>
									<li><a href="o_list">Govt.Services </a></li>
									<li><a href="o_list">Doctors </a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="#">State</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="s_list">State 1</a></li>
									<li><a href="s_list">State 2</a></li>
									<li><a href="s_list">State 3</a></li>
									<li><a href="s_list">State 4</a></li>
									<li><a href="s_list">State 5</a></li>
									<li><a href="s_list">State 6</a></li>
									<li><a href="s_list">State 7</a></li>
									<li><a href="s_list">State 8</a></li>
									<li><a href="s_list">State 9</a></li>
									<li><a href="s_list">State 10</a></li>
									<li><a href="s_list">State 11</a></li>
									<li><a href="s_list">State 12</a></li>
									<li><a href="s_list">State 13</a></li>
									<li><a href="s_list">State 14</a></li>
									<li><a href="s_list">State 15</a></li>
									<li><a href="s_list">State 16</a></li>
									<li><a href="s_list">State 17</a></li>
									<li><a href="s_list">State 18</a></li>
									<li><a href="s_list">State 19</a></li>
									<li><a href="s_list">State 20</a></li>
									<li><a href="s_list">State 21</a></li>
									<li><a href="s_list">State 22</a></li>
									<li><a href="s_list">State 23</a></li>
									<li><a href="s_list">State 24</a></li>
									<li><a href="s_list">State 25</a></li>
									<li><a href="s_list">State 26</a></li>
									<li><a href="s_list">State 27</a></li>
									<li><a href="s_list">State 28</a></li>
									<li><a href="s_list">State 29</a></li>
									<li><a href="s_list">State 30</a></li>
									<li><a href="s_list">State 31</a></li>
									<li><a href="s_list">State 32</a></li>
									<li><a href="s_list">State 33</a></li>
									<li><a href="s_list">State 34</a></li>
									<li><a href="s_list">State 35</a></li>
									<li><a href="s_list">State 36</a></li>
									<li><a href="s_list">State 37</a></li>
									<li><a href="s_list">State 38</a></li>
									<li><a href="s_list">State 39</a></li>
									<li><a href="s_list">State 40</a></li>
									<li><a href="s_list">State 41</a></li>
									<li><a href="s_list">State 42</a></li>
									<li><a href="s_list">State 43</a></li>
									<li><a href="s_list">State 44</a></li>
									<li><a href="s_list">State 45</a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
							<!-- .has-children -->
							<li class="has-children"><a href="#">NRI</a>
								<ul class="cd-secondary-dropdown is-hidden">
									<li class="go-back"><a href="#">Menu</a></li>
									<li><a href="nri_list">Country 1</a></li>
									<li><a href="nri_list">Country 2</a></li>
									<li><a href="nri_list">Country 3</a></li>
									<li><a href="nri_list">Country 4</a></li>
									<li><a href="nri_list">Country 5</a></li>
									<li><a href="nri_list">Country 6</a></li>
									<li><a href="nri_list">Country 7</a></li>
									<li><a href="nri_list">Country 8</a></li>
									<li><a href="nri_list">Country 9</a></li>
									<li><a href="nri_list">Country 10</a></li>
									<li><a href="nri_list">Country 11</a></li>
									<li><a href="nri_list">Country 12</a></li>
								</ul> <!-- .cd-secondary-dropdown --></li>
						</ul>
						<!-- .cd-dropdown-content --> </nav>
						<!-- .cd-dropdown -->
					</div>
					<!-- .cd-dropdown-wrapper -->
				</div>
				<div class="pull-right">
					<nav class="navbar nav_bottom" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header nav_2">
						<button type="button"
							class="navbar-toggle collapsed navbar-toggle1"
							data-toggle="collapse" data-target="#bs-megadropdown-tabs">
							Menu <span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
						<ul class="nav navbar-nav nav_1">
							<li><a href="indexPage">Home</a></li>
							<li><a href="about">About</a></li>
							<li><a href="search">Search</a></li>
							<li><a href="app" target="_blank">Mobile</a></li>
							<!--<li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Search<span class="caret"></span></a>
		              <ul class="dropdown-menu" role="menu">
		                <li><a href="search">Regular Search</a></li>
		                <li><a href="profile">Recently Viewed Profiles</a></li>
		                <li><a href="search-id">Search By Profile ID</a></li>
		                <li><a href="faq">Faq</a></li>
		                <li><a href="shortcodes">Shortcodes</a></li>
		              </ul>
		            </li>-->
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown">Quick Search<span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<div class="banner-bottom-login">
										<div class="w3agile_banner_btom_login">
											<form action="#" method="post">
												<div class="w3agile__text w3agile_banner_btom_login_left">
													<h4>I'm looking for a</h4>
													<select id="country" onchange="change_country(this.value)"
														class="frm-field required">
														<option value="null">Bride</option>
														<option value="null">Groom</option>
													</select>
												</div>
												<div class="w3agile__text w3agile_banner_btom_login_left1">
													<h4>Aged</h4>
													<select id="country1" onchange="change_country(this.value)"
														class="frm-field required">
														<option value="null">20</option>
														<option value="null">21</option>
														<option value="null">22</option>
														<option value="null">23</option>
														<option value="null">24</option>
														<option value="null">25</option>
														<option value="null">- - -</option>
													</select> <span>To </span> <select id="country2"
														onchange="change_country(this.value)"
														class="frm-field required">
														<option value="null">30</option>
														<option value="null">31</option>
														<option value="null">32</option>
														<option value="null">33</option>
														<option value="null">34</option>
														<option value="null">35</option>
														<option value="null">- - -</option>
													</select>
												</div>
												<div class="w3agile__text w3agile_banner_btom_login_left2">
													<h4>Religion</h4>
													<select id="country3" onchange="change_country(this.value)"
														class="frm-field required">
														<option value="null">Hindu</option>
														<option value="null">Muslim</option>
														<option value="null">Christian</option>
														<option value="null">Sikh</option>
														<option value="null">Jain</option>
														<option value="null">Buddhist</option>
														<option value="null">No Religious Belief</option>
													</select>
												</div>
												<div class="w3agile_banner_btom_login_left3">
													<input type="submit" value="Search" />
												</div>
												<div class="clearfix"></div>
											</form>
										</div>
									</div>
								</ul></li>
							<li class="last"><a href="contact">Contacts</a></li>
						</ul>
					</div>
					<!-- /.navbar-collapse --> </nav>
				</div>
				<!-- end pull-right -->
				<div class="clearfix"></div>
			</div>
			<!-- end container -->
		</div>
		<!-- end navbar-inner -->
	</div>
	<!-- end navbar-inverse-blue --> <!-- ============================  Navigation End ============================ -->
	</header>
	<!-- /header -->
</body>
</html>